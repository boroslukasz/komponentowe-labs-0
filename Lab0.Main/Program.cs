﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{


    public class samochod
    {
        protected string silnik;
        protected int spalanie;
        protected int liczbaPoduszek;
        protected int pojemnosc;

        public string Silnik
        {
            get
            {
                return silnik;
            }
            set
            {
                silnik = value;

            }
        }

        public int Spalanie
        {
            get
            {
                return spalanie;
            }
            set
            {
                spalanie = value;

            }
        }

        public int LiczbaPoduszek
        {
            get
            {
                return liczbaPoduszek;
            }
            set
            {
                liczbaPoduszek = value;

            }
        }

        public int Pojemnosc
        {
            get
            {
                return pojemnosc;
            }
            set
            {
                pojemnosc = value;

            }
        }

        public samochod()
        { 
        }
        
        public samochod(string silnik, int spalanie, int liczbaPoduszek, int pojemnosc)
        {
            this.silnik = silnik;
            this.spalanie = spalanie;
            this.liczbaPoduszek= liczbaPoduszek;
            this.pojemnosc = pojemnosc;
        }
        
        public virtual string INFO()
        {
            string informacja = "";
            informacja = "silnik:" + " " + this.silnik + " " + "spalanie:" + " " + this.spalanie+" "+  "liczba poduszek:" + " " + this.liczbaPoduszek+" "+ "pojemnosc:" + " "+ this.pojemnosc;
            return informacja;
        }
       

    }

    class osobowy : samochod
    {
        protected int pojemnoscBagaznika;

        public osobowy() : base() 
        { 
        }

        public osobowy(string silnik, int spalanie, int liczbaPoduszek, int pojemnosc, int pojemnoscBagaznika): base (silnik,spalanie,liczbaPoduszek,pojemnosc)
        {
           
            this.pojemnoscBagaznika = pojemnoscBagaznika;

       }

        public override string INFO()
        {
            return base.INFO() + this.pojemnoscBagaznika;
        }

       
    }

    
    
    class ciezarowy : samochod
    {
        protected bool CzyNaczepy;
        
        public ciezarowy() : base() 
        { 
        }
 
        public ciezarowy(string silnik, int spalanie, int liczbaPoduszek, int pojemnosc, bool CzyNaczepy)
             : base(silnik, spalanie, liczbaPoduszek, pojemnosc)
        {
            this.CzyNaczepy = CzyNaczepy;
        }
        
      
        public override string INFO()
        {
            return base.INFO()+" "+ "czy ma naczepy:" +" "+ this.CzyNaczepy;
        }
    }





    class Program
    {
        static void Main(string[] args)
        {
            osobowy mojsamochod = new osobowy("benzynowy", 8, 4, 2000, 50);
            Console.WriteLine(mojsamochod.INFO());

            ciezarowy mojciezarowy = new ciezarowy("disel", 19, 2, 8000, true);
            Console.WriteLine(mojciezarowy.INFO());

            Console.ReadKey();
        }
    }
}
